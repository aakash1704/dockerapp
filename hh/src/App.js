import axios from "axios";
import "./App.css";
import { useState } from "react";

function App() {
  const [search, setSearch] = useState('Awaiting response...')
  const [names, setNames] = useState("");
  const handleClick = (e) => {
    e.preventDefault();
    axios
      .post("http://localhost:8080/data", { data:names })
      .then((res) => {
        console.log(res.data.dbdata)
        const matchedkey = res.data.dbdata
        setSearch(matchedkey)
      });
      
  };
  return (
    <div className="App">
      <div className="container_box">
        <h1>Data Query</h1>
        <input
          type="text"
          placeholder="Enter your name"
          onChange={(e) => {
            setNames(e.target.value);
          }}
        />
        <button onClick={(e) => handleClick(e)}>Submit</button>
      </div>
      <div className="container_box">
        <h1>{JSON.stringify(search)}</h1>
        
        {/* <input type="input" placeholder="Awaiting response" /> */}
        {/* <button onClick={(e) => handleClick(e)}>Submit</button> */}
      </div>
    </div>
  );
}

export default App;
