package routers

import (
	"net/http"

	"docker/middleware"
	"docker/models"

	"github.com/gin-gonic/gin"
)

type CreateUserData struct {
	Data string `json:"data"`
}

func RegisterRoutes() *gin.Engine {
	router := gin.Default()
	router.Use(middleware.CORSMiddleware())

	//router.GET("/keydata", getData)
	router.POST("/data", postData)

	return router
}

/*func getData(c *gin.Context) {
	var allKeys []models.Key
	fmt.Println(c.Request.Body)
	models.DB.Where("keyword <> ?", dbReq)

	c.IndentedJSON(http.StatusOK, gin.H{"dbdata": allKeys})
}*/

func postData(c *gin.Context) {
	var newdata CreateUserData
	if err := c.ShouldBindJSON(&newdata); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	dbReq := newdata.Data
	kw := models.User{Data: &newdata.Data}
	models.DB.Create(&kw)

	//c.IndentedJSON(http.StatusCreated, gin.H{"postdata": kw})

	var allUser []models.User
	models.DB.Where("Data = ?", dbReq).Find(&allUser)

	c.IndentedJSON(http.StatusOK, gin.H{"dbdata": allUser})
}
