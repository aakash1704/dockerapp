package main

import (
	"docker/models"
	"docker/routers"
	"fmt"

	_ "github.com/lib/pq"
)

func main() {
	r := routers.RegisterRoutes()
	models.ConnectDatabase()
	fmt.Printf("successfully connected")

	// r.Post("/submit", controllers.submit)
	// r.Get("/retrieve", controllers.retrieve)

	r.Run("localhost:8080")

}
